from time import time, sleep


class Timer:
    def __init__(self):
        pass

    def __enter__(self):
        self.start = time()
        # return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.end = time()
        t = self.end - self.start
        print(t)


with Timer():
    sleep(1)

# print(x2)
