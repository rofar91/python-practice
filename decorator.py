from datetime import datetime
import functools


def timeit(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start = datetime.now()
        result = func(*args, **kwargs)
        print('Время выполнения функции - ', (datetime.now() - start))
        return result
    return wrapper


@timeit
def gen_even_list(n):
    evenlist = [x for x in range(n) if x % 2 == 0]
    return evenlist


gen_even_list(1000000)
